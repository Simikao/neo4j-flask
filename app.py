from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os  # provides ways to access the Operating System and allows us to read the environment variables

# import logging
# logging.basicConfig(level=logging.DEBUG)c

load_dotenv()

app = Flask(__name__)

uri = os.getenv("URI")
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(user, password), database="neo4j")


def get_employees(tx, filter_params):
    query = "MATCH (m:Employee)"
    if filter_params:
        query += " WHERE "
        conditions = []
        for key, value in filter_params.items():
            conditions.append(f"m.{key} = '{value}'")
        query += " AND ".join(conditions)
    query += " RETURN m "
    results = tx.run(query).data()
    employees = [{"result": result["m"]} for result in results]
    return employees


@app.route("/employees", methods=["GET"])
def get_employees_route():
    try:
        filter_params = request.args.to_dict()
        with driver.session() as session:
            employees = session.read_transaction(
                get_employees, filter_params=filter_params
            )

        response = {"employees": employees}
        return jsonify(response)
    except Exception as err:
        return jsonify({"error": str(err)}), 500


def get_employee_id(tx, firstName, lastName):
    query = "MATCH (employeeID:Employee {firstName: $firstName, lastName: $lastName}) RETURN ID(employeeID)"
    result = tx.run(query, firstName=firstName, lastName=lastName).data()
    return result


@app.route("/employees/id", methods=["GET"])
def get_employee_id_route():
    firstName = request.json["firstName"]
    lastName = request.json["lastName"]
    with driver.session() as session:
        number = session.read_transaction(get_employee_id, firstName, lastName)

    if not number:
        response = {"message": "Employee not found"}
        return jsonify(response), 404
    else:
        return number


def check_uniqueness(tx, firstName, lastName):
    query = (
        "MATCH (e:Employee {firstName: $firstName, lastName: $lastName}) "
        "RETURN COUNT(e) AS count"
    )
    result = tx.run(query, firstName=firstName, lastName=lastName).single()
    return result["count"] == 0


def add_employee(tx, firstName, lastName, position, department):
    query = (
        "MATCH (m:Employee {position: $dean})-[:WORKS_IN]->(d:Department {name: $department}) "
        "CREATE (m)-[:MANAGES]->(e:Employee {firstName: $firstName, lastName: $lastName, position: $position})-[:WORKS_IN]->(d)"
    )
    tx.run(
        query,
        firstName=firstName,
        lastName=lastName,
        position=position,
        department=department,
        dean="dean",
    )


@app.route("/employees", methods=["POST"])
def add_employee_route():
    firstName = request.json["firstName"]
    lastName = request.json["lastName"]
    position = request.json["position"]
    department = request.json["department"]
    with driver.session() as session:
        if not [x for x in (firstName, lastName, position, department) if x is None]:
            if session.read_transaction(check_uniqueness, firstName, lastName):
                session.write_transaction(
                    add_employee, firstName, lastName, position, department
                )
                response = {"status": "success"}
            else:
                response = {"status": "Halted - That person exists"}
        else:
            response = {"status": "Missing parameters"}
    return jsonify(response)


def update_employee(
    tx, employeeID, new_firstName, new_lastName, new_position, new_department
):
    query_match = "MATCH (e:Employee) WHERE ID(e)=$employeeID RETURN e"
    result = tx.run(query_match, employeeID=employeeID).data()

    if not result:
        return False
    else:
        query_update = "MATCH (e:Employee WHERE ID(e)=$employeeID)"
        set_clauses = []

        if new_firstName is not None:
            set_clauses.append("e.firstName = $new_firstName")
        if new_lastName is not None:
            set_clauses.append("e.lastName = $new_lastName")
        if new_position is not None:
            set_clauses.append("e.position = $new_position")
        if len(set_clauses) > 0:
            query_update += " SET " + ", ".join(set_clauses)

        tx.run(
            query_update,
            employeeID=employeeID,
            new_firstName=new_firstName,
            new_lastName=new_lastName,
            new_position=new_position,
        )

        if new_department is not None:
            query_delete_relationships = (
                "MATCH (e:Employee)-[r]-() WHERE ID(e)=$employeeID DELETE r"
            )
            tx.run(query_delete_relationships, employeeID=employeeID)

            query_create_relationships = (
                "MATCH (m:Employee {position:$dean})-[:WORKS_IN]->(d:Department{name:$new_department}),"
                "(e:Employee) WHERE ID(e)=$employeeID CREATE (e)-[:WORKS_IN]->(d), "
                "(m)-[:MANAGES]->(e)"
            )
            tx.run(
                query_create_relationships,
                employeeID=employeeID,
                new_department=new_department,
                dean="dean",
            )

        return True


@app.route("/employees/<int:employeeID>", methods=["PUT"])
def update_employee_route(employeeID):
    new_firstName = request.json["firstName"]
    new_lastName = request.json["lastName"]
    new_position = request.json["position"]
    new_department = request.json["department"]

    with driver.session() as session:
        employee = session.write_transaction(
            update_employee,
            employeeID=employeeID,
            new_firstName=new_firstName,
            new_lastName=new_lastName,
            new_position=new_position,
            new_department=new_department,
        )

    if not employee:
        response = {"message": "No employee of that ID"}
        return jsonify(response), 404
    else:
        response = {"status": "success"}
        return jsonify(response)


def delete_employee(tx, employeeID):
    query = "MATCH (e:Employee) WHERE ID(e)=$employeeID RETURN e"
    result = tx.run(query, employeeID=employeeID).data()

    if not result:
        return False
    else:
        query = "MATCH (e:Employee) WHERE ID(e)=$employeeID DETACH DELETE e"
        tx.run(query, employeeID=employeeID)
        return True


@app.route("/employees/<int:employeeID>", methods=["DELETE"])
def delete_employee_route(employeeID):
    with driver.session() as session:
        employee = session.write_transaction(delete_employee, employeeID)

    if not employee:
        response = {"message": "Employee not found"}
        return jsonify(response), 404
    else:
        response = {"status": "success"}
        return jsonify(response)


def find_subordinates(tx, employeeID):
    query = "MATCH (e:Employee) WHERE ID(e)=$employeeID RETURN e"
    result = tx.run(query, employeeID=employeeID).data()

    if not result:
        return None
    else:
        query = "MATCH(m:Employee)-[:MANAGES]->(e:Employee) WHERE ID(m)=$employeeID return e"
        results = tx.run(query, employeeID=employeeID)
        employees = [{"result": dict(result["e"])} for result in results]
        return employees


@app.route("/employees/<int:employeeID>/subordinates", methods=["GET"])
def find_subordinates_route(employeeID):
    with driver.session() as session:
        subordinates = session.write_transaction(find_subordinates, employeeID)

    if not subordinates:
        response = {"message": "Manager of that ID not found"}
        return jsonify(response), 404
    else:
        response = {"subordinates": subordinates}
        return jsonify(response)


def get_departments(tx, filter_value, sort_order):
    query = "MATCH (d:Department) "

    if filter_value:
        query += " WHERE d.name CONTAINS $filter_value"
    query += " RETURN d "
    if sort_order != "desc":
        sort_order = "asc"
    query += f" ORDER BY d.name {sort_order}"

    results = tx.run(query, filter_value=filter_value, sort_order=sort_order).data()

    departments = [{"name": result["d"]["name"]} for result in results]
    return departments


@app.route("/departments", methods=["GET"])
def get_departments_route():
    try:
        filter_value = request.args.get("filter_value")
        sort_order = request.args.get("sort_order")
        with driver.session() as session:
            departments = session.read_transaction(
                get_departments, filter_value, sort_order
            )

        response = {"departments": departments}
        return jsonify(response)
    except Exception as e:
        return jsonify({"error": str(e)}), 500


def get_department_id(tx, name):
    query = "MATCH (departmentID:Department {name:$name}) RETURN ID(departmentID)"
    result = tx.run(query, name=name).data()
    return result


@app.route("/departments/id", methods=["GET"])
def get_department_id_route():
    name = request.args.get("name")
    with driver.session() as session:
        number = session.read_transaction(get_department_id, name)

    if not number:
        response = {"message": "Department not found"}
        return jsonify(response), 404
    else:
        return number


def find_department_employees(tx, departmentID):
    query = "MATCH (d:Department) WHERE ID(d)=$departmentID RETURN d"
    result = tx.run(query, departmentID=departmentID).data()

    if not result:
        return None
    else:
        query = (
            "MATCH (d:Department)<-[r:WORKS_IN]-(e:Employee)WHERE ID(d)=$departmentID "
            "RETURN e ORDER BY e.position ASC"
        )
        results = tx.run(query, departmentID=departmentID)
        employees = [{"result": dict(result["e"])} for result in results]
        return employees


@app.route("/employees/<int:departmentID>/employees", methods=["GET"])
def find_department_employees_route(departmentID):
    with driver.session() as session:
        employees = session.write_transaction(find_department_employees, departmentID)

    if not employees:
        response = {"message": "Department not found"}
        return jsonify(response), 404
    else:
        response = {"employees": employees}
        return jsonify(response)


if __name__ == "__main__":
    app.run()
